package com.example.microservicetraceability.application.mapper;

import com.example.microservicetraceability.application.dto.TraceabilityRequest;
import com.example.microservicetraceability.domain.model.Traceability;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-08-25T09:26:22-0500",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 19.0.2 (Amazon.com Inc.)"
)
@Component
public class TraceabilityRequestMapperImpl implements TraceabilityRequestMapper {

    @Override
    public Traceability toModel(TraceabilityRequest traceabilityRequest) {
        if ( traceabilityRequest == null ) {
            return null;
        }

        Traceability traceability = new Traceability();

        traceability.setId( traceabilityRequest.getId() );
        traceability.setOrderId( traceabilityRequest.getOrderId() );
        traceability.setCustomerId( traceabilityRequest.getCustomerId() );
        traceability.setEmailCustomer( traceabilityRequest.getEmailCustomer() );
        traceability.setDate( traceabilityRequest.getDate() );
        traceability.setStateBefore( traceabilityRequest.getStateBefore() );
        traceability.setStateCurrent( traceabilityRequest.getStateCurrent() );
        traceability.setEmployeeId( traceabilityRequest.getEmployeeId() );
        traceability.setEmailEmployee( traceabilityRequest.getEmailEmployee() );

        return traceability;
    }
}
