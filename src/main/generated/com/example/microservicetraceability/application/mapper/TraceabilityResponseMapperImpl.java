package com.example.microservicetraceability.application.mapper;

import com.example.microservicetraceability.application.dto.TraceabilityResponse;
import com.example.microservicetraceability.domain.model.Traceability;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-08-25T09:26:22-0500",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 19.0.2 (Amazon.com Inc.)"
)
@Component
public class TraceabilityResponseMapperImpl implements TraceabilityResponseMapper {

    @Override
    public TraceabilityResponse toResponse(Traceability traceability) {
        if ( traceability == null ) {
            return null;
        }

        TraceabilityResponse traceabilityResponse = new TraceabilityResponse();

        traceabilityResponse.setId( traceability.getId() );
        traceabilityResponse.setOrderId( traceability.getOrderId() );
        traceabilityResponse.setCustomerId( traceability.getCustomerId() );
        traceabilityResponse.setEmailCustomer( traceability.getEmailCustomer() );
        traceabilityResponse.setDate( traceability.getDate() );
        traceabilityResponse.setStateBefore( traceability.getStateBefore() );
        traceabilityResponse.setStateCurrent( traceability.getStateCurrent() );
        traceabilityResponse.setEmployeeId( traceability.getEmployeeId() );
        traceabilityResponse.setEmailEmployee( traceability.getEmailEmployee() );

        return traceabilityResponse;
    }

    @Override
    public List<TraceabilityResponse> toResponses(List<Traceability> traceabilities) {
        if ( traceabilities == null ) {
            return null;
        }

        List<TraceabilityResponse> list = new ArrayList<TraceabilityResponse>( traceabilities.size() );
        for ( Traceability traceability : traceabilities ) {
            list.add( toResponse( traceability ) );
        }

        return list;
    }
}
