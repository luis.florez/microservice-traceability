package com.example.microservicetraceability.application.mapper;

import com.example.microservicetraceability.application.dto.TraceabilityTimeResponse;
import com.example.microservicetraceability.domain.model.TraceabilityTime;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-08-25T15:12:07-0500",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 19.0.2 (Amazon.com Inc.)"
)
@Component
public class TraceabilityTimeResponseMapperImpl implements TraceabilityTimeResponseMapper {

    @Override
    public TraceabilityTimeResponse toResponse(TraceabilityTime traceabilityTime) {
        if ( traceabilityTime == null ) {
            return null;
        }

        TraceabilityTimeResponse traceabilityTimeResponse = new TraceabilityTimeResponse();

        traceabilityTimeResponse.setOrderId( traceabilityTime.getOrderId() );
        traceabilityTimeResponse.setTime( traceabilityTime.getTime() );

        return traceabilityTimeResponse;
    }

    @Override
    public List<TraceabilityTimeResponse> toResponses(List<TraceabilityTime> traceabilityTimes) {
        if ( traceabilityTimes == null ) {
            return null;
        }

        List<TraceabilityTimeResponse> list = new ArrayList<TraceabilityTimeResponse>( traceabilityTimes.size() );
        for ( TraceabilityTime traceabilityTime : traceabilityTimes ) {
            list.add( toResponse( traceabilityTime ) );
        }

        return list;
    }
}
