package com.example.microservicetraceability.application.mapper;

import com.example.microservicetraceability.application.dto.UserRankingResponse;
import com.example.microservicetraceability.domain.model.UserRanking;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-08-25T15:12:07-0500",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 19.0.2 (Amazon.com Inc.)"
)
@Component
public class UserRankingResponseMapperImpl implements UserRankingResponseMapper {

    @Override
    public UserRankingResponse toResponse(UserRanking userRanking) {
        if ( userRanking == null ) {
            return null;
        }

        UserRankingResponse userRankingResponse = new UserRankingResponse();

        userRankingResponse.setEmployeeId( userRanking.getEmployeeId() );
        userRankingResponse.setAverageTime( userRanking.getAverageTime() );

        return userRankingResponse;
    }

    @Override
    public List<UserRankingResponse> toResponses(List<UserRanking> userRankings) {
        if ( userRankings == null ) {
            return null;
        }

        List<UserRankingResponse> list = new ArrayList<UserRankingResponse>( userRankings.size() );
        for ( UserRanking userRanking : userRankings ) {
            list.add( toResponse( userRanking ) );
        }

        return list;
    }
}
