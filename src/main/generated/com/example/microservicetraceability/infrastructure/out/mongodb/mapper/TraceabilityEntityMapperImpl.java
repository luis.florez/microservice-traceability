package com.example.microservicetraceability.infrastructure.out.mongodb.mapper;

import com.example.microservicetraceability.domain.model.Traceability;
import com.example.microservicetraceability.infrastructure.out.mongodb.entity.TraceabilityEntity;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-08-25T09:26:22-0500",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 19.0.2 (Amazon.com Inc.)"
)
@Component
public class TraceabilityEntityMapperImpl implements TraceabilityEntityMapper {

    @Override
    public TraceabilityEntity toEntity(Traceability traceability) {
        if ( traceability == null ) {
            return null;
        }

        TraceabilityEntity traceabilityEntity = new TraceabilityEntity();

        traceabilityEntity.setId( traceability.getId() );
        traceabilityEntity.setOrderId( traceability.getOrderId() );
        traceabilityEntity.setCustomerId( traceability.getCustomerId() );
        traceabilityEntity.setEmailCustomer( traceability.getEmailCustomer() );
        traceabilityEntity.setDate( traceability.getDate() );
        traceabilityEntity.setStateBefore( traceability.getStateBefore() );
        traceabilityEntity.setStateCurrent( traceability.getStateCurrent() );
        traceabilityEntity.setEmployeeId( traceability.getEmployeeId() );
        traceabilityEntity.setEmailEmployee( traceability.getEmailEmployee() );

        return traceabilityEntity;
    }

    @Override
    public Traceability toModel(TraceabilityEntity traceabilityEntity) {
        if ( traceabilityEntity == null ) {
            return null;
        }

        Traceability traceability = new Traceability();

        traceability.setId( traceabilityEntity.getId() );
        traceability.setOrderId( traceabilityEntity.getOrderId() );
        traceability.setCustomerId( traceabilityEntity.getCustomerId() );
        traceability.setEmailCustomer( traceabilityEntity.getEmailCustomer() );
        traceability.setDate( traceabilityEntity.getDate() );
        traceability.setStateBefore( traceabilityEntity.getStateBefore() );
        traceability.setStateCurrent( traceabilityEntity.getStateCurrent() );
        traceability.setEmployeeId( traceabilityEntity.getEmployeeId() );
        traceability.setEmailEmployee( traceabilityEntity.getEmailEmployee() );

        return traceability;
    }

    @Override
    public List<Traceability> toModels(List<TraceabilityEntity> traceabilityEntities) {
        if ( traceabilityEntities == null ) {
            return null;
        }

        List<Traceability> list = new ArrayList<Traceability>( traceabilityEntities.size() );
        for ( TraceabilityEntity traceabilityEntity : traceabilityEntities ) {
            list.add( toModel( traceabilityEntity ) );
        }

        return list;
    }
}
