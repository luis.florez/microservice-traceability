package com.example.microservicetraceability;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class MicroserviceTraceabilityApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroserviceTraceabilityApplication.class, args);
	}

}
