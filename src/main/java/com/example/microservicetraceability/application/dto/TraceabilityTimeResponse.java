package com.example.microservicetraceability.application.dto;

public class TraceabilityTimeResponse {

    private Long orderId;
    private Long time;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }
}
