package com.example.microservicetraceability.application.handler.impl;

import com.example.microservicetraceability.application.dto.TraceabilityRequest;
import com.example.microservicetraceability.application.dto.TraceabilityResponse;
import com.example.microservicetraceability.application.dto.TraceabilityTimeResponse;
import com.example.microservicetraceability.application.dto.UserRankingResponse;
import com.example.microservicetraceability.application.handler.ITraceabilityHandler;
import com.example.microservicetraceability.application.mapper.TraceabilityRequestMapper;
import com.example.microservicetraceability.application.mapper.TraceabilityResponseMapper;
import com.example.microservicetraceability.application.mapper.TraceabilityTimeResponseMapper;
import com.example.microservicetraceability.application.mapper.UserRankingResponseMapper;
import com.example.microservicetraceability.domain.api.ITraceabilityServicePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class TraceabilityHandler implements ITraceabilityHandler {

    private final ITraceabilityServicePort traceabilityServicePort;
    private final TraceabilityRequestMapper traceabilityRequestMapper;
    private final TraceabilityResponseMapper traceabilityResponseMapper;
    private final TraceabilityTimeResponseMapper traceabilityTimeResponseMapper;
    private final UserRankingResponseMapper userRankingResponseMapper;

    @Override
    public void saveTraceability(TraceabilityRequest traceabilityRequest) {
        traceabilityServicePort.saveTraceability(traceabilityRequestMapper.toModel(traceabilityRequest));
    }

    @Override
    public List<TraceabilityResponse> getTraceabilityByOrderAndCustomer(String token, Long orderId) {
        return traceabilityResponseMapper.toResponses(traceabilityServicePort.getTraceabilityByOrderAndCustomer(token, orderId));
    }

    @Override
    public List<TraceabilityTimeResponse> getTraceabilityTimesOrders(String token, Long restaurantId) {
        return traceabilityTimeResponseMapper.toResponses(traceabilityServicePort.getTraceabilityTimesOrders(token, restaurantId));
    }

    @Override
    public List<UserRankingResponse> getRankingEmployees(String token, Long restaurantId) {
        return userRankingResponseMapper.toResponses(traceabilityServicePort.getRankingEmployees(token, restaurantId));
    }
}
