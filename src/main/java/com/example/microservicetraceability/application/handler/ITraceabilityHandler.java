package com.example.microservicetraceability.application.handler;

import com.example.microservicetraceability.application.dto.TraceabilityRequest;
import com.example.microservicetraceability.application.dto.TraceabilityResponse;
import com.example.microservicetraceability.application.dto.TraceabilityTimeResponse;
import com.example.microservicetraceability.application.dto.UserRankingResponse;

import java.util.List;

public interface ITraceabilityHandler {

    void saveTraceability(TraceabilityRequest traceabilityRequest);

    List<TraceabilityResponse> getTraceabilityByOrderAndCustomer(String token, Long orderId);

    List<TraceabilityTimeResponse> getTraceabilityTimesOrders(String token, Long restaurantId);

    List<UserRankingResponse> getRankingEmployees(String token, Long restaurantId);
}
