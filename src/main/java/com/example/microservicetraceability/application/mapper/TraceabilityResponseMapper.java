package com.example.microservicetraceability.application.mapper;

import com.example.microservicetraceability.application.dto.TraceabilityResponse;
import com.example.microservicetraceability.domain.model.Traceability;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TraceabilityResponseMapper {

    TraceabilityResponse toResponse(Traceability traceability);

    List<TraceabilityResponse> toResponses(List<Traceability> traceabilities);
}
