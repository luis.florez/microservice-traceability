package com.example.microservicetraceability.application.mapper;

import com.example.microservicetraceability.application.dto.TraceabilityRequest;
import com.example.microservicetraceability.domain.model.Traceability;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TraceabilityRequestMapper {

    Traceability toModel(TraceabilityRequest traceabilityRequest);
}
