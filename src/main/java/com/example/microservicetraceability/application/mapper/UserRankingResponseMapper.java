package com.example.microservicetraceability.application.mapper;

import com.example.microservicetraceability.application.dto.UserRankingResponse;
import com.example.microservicetraceability.domain.model.UserRanking;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserRankingResponseMapper {

    UserRankingResponse toResponse(UserRanking userRanking);

    List<UserRankingResponse> toResponses(List<UserRanking> userRankings);
}
