package com.example.microservicetraceability.application.mapper;

import com.example.microservicetraceability.application.dto.TraceabilityTimeResponse;
import com.example.microservicetraceability.domain.model.TraceabilityTime;
import org.mapstruct.Mapper;
import java.util.List;

@Mapper(componentModel = "spring")
public interface TraceabilityTimeResponseMapper {

    TraceabilityTimeResponse toResponse(TraceabilityTime traceabilityTime);

    List<TraceabilityTimeResponse> toResponses(List<TraceabilityTime> traceabilityTimes);
}
