package com.example.microservicetraceability.infrastructure.constant;

public class SecurityConstant {

    private SecurityConstant() {
        throw new IllegalStateException(EXCEPTION_UTILITY_CLASS);
    }

    public static final String EXCEPTION_UTILITY_CLASS = "Utility class";
    public static final String AUTHORIZATION_FOR_HEADER = "Authorization";
    public static final String VALUE_FOR_AUTHORIZATION_OF_HEADER = "Bearer ";
    public static final String EMPTY = "";
    public static final String TOKEN_SECRET = "eyJhbGciOiJIUzI4gRG9lIiwiaWF0IjoxNTE2MjM5MD2QT4fwpMeJf36POk6yJdQssw5c";
    public static final Long TOKEN_SECRET_SECONDS = 2_059_000L;
    public static final Long SECONDS_TOKEN_PER_THOUSAND = 1000L;
    public static final String CLAIM_DATA_NAME = "name";
    public static final String CLAIM_DATA_ID = "id";
    public static final String CLAIM_DATA_ROLE = "role";
    public static final String ID_ROLE_ZERO = "0";
    public static final String ID_ROLE_ONE = "1";
    public static final String ID_ROLE_TWO = "2";
    public static final String ID_ROLE_THREE = "3";
    public static final String ROLE_ADMINISTRATOR = "ROLE_ADMINISTRATOR";
    public static final String ROLE_PROPRIETARY = "ROLE_PROPRIETARY";
    public static final String ROLE_EMPLOYEE = "ROLE_EMPLOYEE";
    public static final String ROLE_CUSTOMER = "ROLE_CUSTOMER";
    public static final String USER_NOT_FOUND = "User not found";
}
