package com.example.microservicetraceability.infrastructure.exceptionhandler;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.FeignException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import java.io.IOException;
import java.net.ConnectException;
import java.util.Collections;
import java.util.Map;

@RestControllerAdvice
public class ControllerAdvice {

    private static final String MESSAGE = "message";

    @ExceptionHandler(ConnectException.class)
    public ResponseEntity<Map<String, String>> handlerConnectException(ConnectException connectException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.SERVER_PROBLEM.getMessage()), HttpStatus.SERVICE_UNAVAILABLE);
    }

    @ExceptionHandler(FeignException.class)
    public ResponseEntity<Map<String, String>> handlerFeignException(FeignException feignException) throws IOException {
        return ResponseEntity.status(feignException.status()).body(new ObjectMapper().readValue(feignException.contentUTF8(), new TypeReference<>() {}));
    }
}
