package com.example.microservicetraceability.infrastructure.exceptionhandler;

public enum ExceptionResponse {

    SERVER_PROBLEM("The server have problems operating");

    private final String message;

    ExceptionResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}