package com.example.microservicetraceability.infrastructure.security;

import com.example.microservicetraceability.application.dto.UserResponse;
import com.example.microservicetraceability.infrastructure.constant.SecurityConstant;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import java.util.Collection;
import java.util.List;

@RequiredArgsConstructor
public class UserDetailsImpl implements UserDetails {

    private final transient UserResponse userResponse;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(userResponse.getIdRole() + SecurityConstant.EMPTY));
    }

    @Override
    public String getPassword() {
        return userResponse.getPassword();
    }

    @Override
    public String getUsername() {
        return userResponse.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public Long getId() {
        return userResponse.getId();
    }

    public String getName() {
        return userResponse.getName();
    }

    public String gerRole() {
        return userResponse.getIdRole() + SecurityConstant.EMPTY;
    }
}
