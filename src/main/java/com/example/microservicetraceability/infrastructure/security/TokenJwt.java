package com.example.microservicetraceability.infrastructure.security;

import com.example.microservicetraceability.infrastructure.constant.SecurityConstant;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TokenJwt {

    private TokenJwt() {
        throw new IllegalStateException(SecurityConstant.EXCEPTION_UTILITY_CLASS);
    }

    public static String createToken(String email, String name, String role, Long id) {
        Map<String, Object> extra = new HashMap<>();
        long expirationTime = SecurityConstant.TOKEN_SECRET_SECONDS * SecurityConstant.SECONDS_TOKEN_PER_THOUSAND;
        Date expirationDate = new Date(System.currentTimeMillis() + expirationTime);
        extra.put(SecurityConstant.CLAIM_DATA_NAME, name);
        extra.put(SecurityConstant.CLAIM_DATA_ID, id);
        if (role.equals(SecurityConstant.ID_ROLE_ZERO)) extra.put(SecurityConstant.CLAIM_DATA_ROLE, SecurityConstant.ROLE_ADMINISTRATOR);
        else if (role.equals(SecurityConstant.ID_ROLE_ONE)) extra.put(SecurityConstant.CLAIM_DATA_ROLE, SecurityConstant.ROLE_PROPRIETARY);
        else if (role.equals(SecurityConstant.ID_ROLE_TWO)) extra.put(SecurityConstant.CLAIM_DATA_ROLE, SecurityConstant.ROLE_EMPLOYEE);
        else if (role.equals(SecurityConstant.ID_ROLE_THREE)) extra.put(SecurityConstant.CLAIM_DATA_ROLE, SecurityConstant.ROLE_CUSTOMER);
        return Jwts.builder()
                .setSubject(email)
                .setExpiration(expirationDate)
                .addClaims(extra)
                .signWith(Keys.hmacShaKeyFor(SecurityConstant.TOKEN_SECRET.getBytes()))
                .compact();
    }

    public static UsernamePasswordAuthenticationToken getAuthentication(String token) {
        try {
            Claims claims = Jwts.parserBuilder()
                    .setSigningKey(SecurityConstant.TOKEN_SECRET.getBytes())
                    .build()
                    .parseClaimsJws(token)
                    .getBody();
            return new UsernamePasswordAuthenticationToken(claims.getSubject(), null, List.<GrantedAuthority>of(new SimpleGrantedAuthority(claims.get(SecurityConstant.CLAIM_DATA_ROLE) + SecurityConstant.EMPTY)));
        } catch (JwtException e) {
            return null;
        }
    }

    public static String getIdToken(String token) {
        try {
            Claims claims = Jwts.parserBuilder()
                    .setSigningKey(SecurityConstant.TOKEN_SECRET.getBytes())
                    .build()
                    .parseClaimsJws(token)
                    .getBody();
            return claims.get(SecurityConstant.CLAIM_DATA_ID) + SecurityConstant.EMPTY;
        } catch (JwtException e) {
            return null;
        }
    }
}
