package com.example.microservicetraceability.infrastructure.security;

import com.example.microservicetraceability.application.dto.UserResponse;
import com.example.microservicetraceability.infrastructure.constant.SecurityConstant;
import com.example.microservicetraceability.infrastructure.out.jpa.adapter.UserFeignClient;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserFeignClient userFeignClient;

    public UserDetailsServiceImpl(UserFeignClient userFeignClient) {
        this.userFeignClient = userFeignClient;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserResponse userResponse = userFeignClient.getUserByEmail(email);
        if (userResponse != null) return new UserDetailsImpl(userResponse);
        else throw new UsernameNotFoundException(SecurityConstant.USER_NOT_FOUND);
    }
}
