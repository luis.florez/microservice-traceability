package com.example.microservicetraceability.infrastructure.security;

import com.example.microservicetraceability.infrastructure.constant.SecurityConstant;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtAuthorizationFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        String bearerToken = request.getHeader(SecurityConstant.AUTHORIZATION_FOR_HEADER);
        if (bearerToken != null && bearerToken.startsWith(SecurityConstant.VALUE_FOR_AUTHORIZATION_OF_HEADER)) {
            String token = bearerToken.replace(SecurityConstant.VALUE_FOR_AUTHORIZATION_OF_HEADER, SecurityConstant.EMPTY);
            UsernamePasswordAuthenticationToken authentication = TokenJwt.getAuthentication(token);

            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
        filterChain.doFilter(request, response);
    }
}
