package com.example.microservicetraceability.infrastructure.configuration;

import com.example.microservicetraceability.domain.api.ITraceabilityServicePort;
import com.example.microservicetraceability.domain.spi.ISecurityPort;
import com.example.microservicetraceability.domain.spi.ITraceabilityPersistencePort;
import com.example.microservicetraceability.domain.usecase.TraceabilityUseCase;
import com.example.microservicetraceability.infrastructure.out.jpa.adapter.OrderFeignClient;
import com.example.microservicetraceability.infrastructure.out.jpa.adapter.UserFeignClient;
import com.example.microservicetraceability.infrastructure.out.mongodb.adapter.SecurityAdapter;
import com.example.microservicetraceability.infrastructure.out.mongodb.adapter.TraceabilityMongodbAdapter;
import com.example.microservicetraceability.infrastructure.out.mongodb.mapper.TraceabilityEntityMapper;
import com.example.microservicetraceability.infrastructure.out.mongodb.repository.ITraceabilityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class BeanConfiguration {

    private final UserFeignClient userFeignClient;
    private final OrderFeignClient orderFeignClient;
    private final ITraceabilityRepository traceabilityRepository;
    private final TraceabilityEntityMapper traceabilityEntityMapper;

    @Bean
    public ITraceabilityPersistencePort traceabilityPersistencePort() {
        return new TraceabilityMongodbAdapter(traceabilityRepository, traceabilityEntityMapper);
    }

    @Bean
    public ITraceabilityServicePort traceabilityServicePort() {
        return new TraceabilityUseCase(traceabilityPersistencePort(), securityPort(), orderFeignClient);
    }

    @Bean
    public ISecurityPort securityPort() {
        return new SecurityAdapter();
    }
}