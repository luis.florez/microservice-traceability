package com.example.microservicetraceability.infrastructure.out.mongodb.repository;

import com.example.microservicetraceability.infrastructure.out.mongodb.entity.TraceabilityEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ITraceabilityRepository extends MongoRepository<TraceabilityEntity, String> {

    List<TraceabilityEntity> findByOrderIdAndCustomerId(Long orderId, Long customerId);

    List<TraceabilityEntity> findByOrderId(Long orderId);
}
