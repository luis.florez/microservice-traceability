package com.example.microservicetraceability.infrastructure.out.mongodb.adapter;

import com.example.microservicetraceability.domain.spi.ISecurityPort;
import com.example.microservicetraceability.infrastructure.constant.SecurityConstant;
import com.example.microservicetraceability.infrastructure.security.TokenJwt;
import lombok.RequiredArgsConstructor;

import java.util.Objects;

@RequiredArgsConstructor
public class SecurityAdapter implements ISecurityPort {

    @Override
    public Long getIdUser(String token) {
        return Long.parseLong(Objects.requireNonNull(TokenJwt.getIdToken(token.replace(SecurityConstant.VALUE_FOR_AUTHORIZATION_OF_HEADER, SecurityConstant.EMPTY))));
    }
}
