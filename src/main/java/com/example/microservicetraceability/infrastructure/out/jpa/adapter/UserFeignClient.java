package com.example.microservicetraceability.infrastructure.out.jpa.adapter;

import com.example.microservicetraceability.application.dto.UserResponse;
import com.example.microservicetraceability.domain.spi.IUserPersistencePort;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "MICROSERVICE-USERS", url = "${url.microservice.users}")
public interface UserFeignClient extends IUserPersistencePort {

    @GetMapping(value = "/users/role/{idUser}")
    Boolean existUserWithRoleOwner(@PathVariable(value = "idUser") Long idUser);

    @GetMapping(value = "/users/exists/{id}")
    Boolean existUserWithId(@PathVariable(value = "id") Long id);

    @GetMapping(value = "/users/username")
    UserResponse getUserByEmail(@RequestParam(value = "email") String email);

    @GetMapping(value = "/users/")
    UserResponse getUserById(@RequestParam(value = "id") Long id);

}
