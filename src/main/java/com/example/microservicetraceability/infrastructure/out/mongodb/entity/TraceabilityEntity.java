package com.example.microservicetraceability.infrastructure.out.mongodb.entity;

import com.example.microservicetraceability.domain.model.StateOrderEnum;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.time.LocalDateTime;

@Document("Traceability")
public class TraceabilityEntity {

    @Id
    private String id;
    private Long orderId;
    private Long customerId;
    private String emailCustomer;
    private LocalDateTime date;
    private StateOrderEnum stateBefore;
    private StateOrderEnum stateCurrent;
    private Long employeeId;
    private String emailEmployee;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getEmailCustomer() {
        return emailCustomer;
    }

    public void setEmailCustomer(String emailCustomer) {
        this.emailCustomer = emailCustomer;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public StateOrderEnum getStateBefore() {
        return stateBefore;
    }

    public void setStateBefore(StateOrderEnum stateBefore) {
        this.stateBefore = stateBefore;
    }

    public StateOrderEnum getStateCurrent() {
        return stateCurrent;
    }

    public void setStateCurrent(StateOrderEnum stateCurrent) {
        this.stateCurrent = stateCurrent;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmailEmployee() {
        return emailEmployee;
    }

    public void setEmailEmployee(String emailEmployee) {
        this.emailEmployee = emailEmployee;
    }
}
