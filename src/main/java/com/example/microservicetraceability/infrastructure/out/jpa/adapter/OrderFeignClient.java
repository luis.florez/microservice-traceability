package com.example.microservicetraceability.infrastructure.out.jpa.adapter;

import com.example.microservicetraceability.domain.spi.IOrdersPersistencePort;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import java.util.List;

@FeignClient(name = "MICROSERVICE-SMALL-SQUARE", url = "${url.microservice.small.square}")
public interface OrderFeignClient extends IOrdersPersistencePort {

    @GetMapping("/orders/{restaurantId}")
    List<Long> getAllOrdersByStateDeliveredAndIdRestaurant(@RequestHeader("Authorization") String token, @PathVariable(value = "restaurantId") Long restaurantId);

    @GetMapping("/orders/{restaurantId}/{employeeId}")
    List<Long> getAllOrdersByStateDeliveredAndIdRestaurantAndIdEmployee(@RequestHeader("Authorization") String token, @PathVariable(value = "restaurantId") Long restaurantId, @PathVariable(value = "employeeId") Long employeeId);

    @GetMapping("/restaurants/employees/{restaurantId}")
    List<Long> getEmployeesByRestaurant(@RequestHeader("Authorization") String token, @PathVariable(value = "restaurantId") Long restaurantId);
}
