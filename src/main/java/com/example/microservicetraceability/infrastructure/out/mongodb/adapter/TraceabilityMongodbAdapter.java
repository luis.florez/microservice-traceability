package com.example.microservicetraceability.infrastructure.out.mongodb.adapter;

import com.example.microservicetraceability.domain.model.Traceability;
import com.example.microservicetraceability.domain.spi.ITraceabilityPersistencePort;
import com.example.microservicetraceability.infrastructure.out.mongodb.mapper.TraceabilityEntityMapper;
import com.example.microservicetraceability.infrastructure.out.mongodb.repository.ITraceabilityRepository;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class TraceabilityMongodbAdapter implements ITraceabilityPersistencePort {

    private final ITraceabilityRepository traceabilityRepository;
    private final TraceabilityEntityMapper traceabilityEntityMapper;

    @Override
    public void saveTraceability(Traceability traceability) {
        traceabilityRepository.save(traceabilityEntityMapper.toEntity(traceability));
    }

    @Override
    public List<Traceability> getTraceabilityByOrderAndCustomer(Long orderId, Long customerId) {
        return traceabilityEntityMapper.toModels(traceabilityRepository.findByOrderIdAndCustomerId(orderId, customerId));
    }

    @Override
    public List<Traceability> getTraceabilityByOrder(Long orderId) {
        return traceabilityEntityMapper.toModels(traceabilityRepository.findByOrderId(orderId));
    }
}
