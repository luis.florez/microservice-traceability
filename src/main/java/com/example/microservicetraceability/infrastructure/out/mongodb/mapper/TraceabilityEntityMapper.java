package com.example.microservicetraceability.infrastructure.out.mongodb.mapper;

import com.example.microservicetraceability.domain.model.Traceability;
import com.example.microservicetraceability.infrastructure.out.mongodb.entity.TraceabilityEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TraceabilityEntityMapper {

    TraceabilityEntity toEntity(Traceability traceability);

    Traceability toModel(TraceabilityEntity traceabilityEntity);

    List<Traceability> toModels(List<TraceabilityEntity> traceabilityEntities);
}
