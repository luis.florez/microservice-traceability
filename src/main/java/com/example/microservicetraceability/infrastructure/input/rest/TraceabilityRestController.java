package com.example.microservicetraceability.infrastructure.input.rest;

import com.example.microservicetraceability.application.dto.TraceabilityRequest;
import com.example.microservicetraceability.application.dto.TraceabilityResponse;
import com.example.microservicetraceability.application.dto.TraceabilityTimeResponse;
import com.example.microservicetraceability.application.dto.UserRankingResponse;
import com.example.microservicetraceability.application.handler.ITraceabilityHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/traceability")
@RequiredArgsConstructor
public class TraceabilityRestController {

    private final ITraceabilityHandler traceabilityHandler;

    @PostMapping("/create-log")
    public ResponseEntity<Void> saveTraceability(@RequestBody TraceabilityRequest traceabilityRequest) {
        traceabilityHandler.saveTraceability(traceabilityRequest);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/logs/{orderId}")
    public ResponseEntity<List<TraceabilityResponse>> getTraceabilityByOrderAndCustomer(@RequestHeader("Authorization") String token, @PathVariable Long orderId) {
        return new ResponseEntity<>(traceabilityHandler.getTraceabilityByOrderAndCustomer(token, orderId), HttpStatus.OK);
    }

    @GetMapping("/times/{restaurantId}")
    public ResponseEntity<List<TraceabilityTimeResponse>> getTraceabilityTimesOrders(@RequestHeader("Authorization") String token, @PathVariable Long restaurantId) {
        return new ResponseEntity<>(traceabilityHandler.getTraceabilityTimesOrders(token, restaurantId), HttpStatus.OK);
    }

    @GetMapping("/ranking/{restaurantId}")
    public ResponseEntity<List<UserRankingResponse>> getRankingEmployees(@RequestHeader("Authorization") String token, @PathVariable Long restaurantId) {
        return new ResponseEntity<>(traceabilityHandler.getRankingEmployees(token, restaurantId), HttpStatus.OK);
    }
}
