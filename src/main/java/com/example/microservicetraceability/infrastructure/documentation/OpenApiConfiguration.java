package com.example.microservicetraceability.infrastructure.documentation;

import com.example.microservicetraceability.infrastructure.constant.OpenApiConfigurationConstant;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfiguration {

    @Bean
    public OpenAPI customOpenApi(){
        return new OpenAPI()
                .components(new Components())
                .info(new Info()
                        .title(OpenApiConfigurationConstant.TITLE)
                        .version(OpenApiConfigurationConstant.APP_VERSION)
                        .description(OpenApiConfigurationConstant.APP_DESCRIPTION)
                        .termsOfService(OpenApiConfigurationConstant.TERMS_OF_SERVICE)
                        .license(new License().name(OpenApiConfigurationConstant.NAME_LICENSE).url(OpenApiConfigurationConstant.URL_LICENSE)));
    }
}