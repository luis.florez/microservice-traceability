package com.example.microservicetraceability.domain.model;

public enum StateOrderEnum {

    EARRING,
    IN_PREPARATION,
    READY,
    DELIVERED,
    CANCELLED;
}
