package com.example.microservicetraceability.domain.model.constant;

public class TraceabilityUseCaseConstant {

    private TraceabilityUseCaseConstant() {
        throw new IllegalStateException(EXCEPTION_UTILITY_CLASS);
    }

    public static final String EXCEPTION_UTILITY_CLASS = "Utility class";
    public static final int LOG_OF_START_ORDER = 0;
    public static final int LOG_OF_FINISH_ORDER = 3;
    public static final int START_COUNT_OF_ORDERS = 0;
    public static final Long START_ACCUMULATOR_OF_TIMES_OF_ORDERS = 0L;
}
