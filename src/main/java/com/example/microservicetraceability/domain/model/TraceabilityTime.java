package com.example.microservicetraceability.domain.model;

public class TraceabilityTime {

    private Long orderId;
    private Long time;

    public TraceabilityTime(Long orderId, Long time) {
        this.orderId = orderId;
        this.time = time;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }
}
