package com.example.microservicetraceability.domain.model;

public class UserRanking {

    private Long employeeId;
    private Long averageTime;

    public UserRanking(Long employeeId, Long averageTime) {
        this.employeeId = employeeId;
        this.averageTime = averageTime;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public Long getAverageTime() {
        return averageTime;
    }

    public void setAverageTime(Long averageTime) {
        this.averageTime = averageTime;
    }
}
