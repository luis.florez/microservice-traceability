package com.example.microservicetraceability.domain.spi;

import com.example.microservicetraceability.application.dto.UserResponse;

public interface IUserPersistencePort {

    Boolean existUserWithRoleOwner(Long idUser);

    Boolean existUserWithId(Long id);

    UserResponse getUserByEmail(String email);

    UserResponse getUserById(Long id);
}
