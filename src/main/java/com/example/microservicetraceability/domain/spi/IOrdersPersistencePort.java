package com.example.microservicetraceability.domain.spi;

import java.util.List;

public interface IOrdersPersistencePort {

    List<Long> getAllOrdersByStateDeliveredAndIdRestaurant(String token, Long restaurantId);

    List<Long> getAllOrdersByStateDeliveredAndIdRestaurantAndIdEmployee(String token, Long restaurantId, Long employeeId);

    List<Long> getEmployeesByRestaurant(String token, Long restaurantId);
}
