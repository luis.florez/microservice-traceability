package com.example.microservicetraceability.domain.spi;

public interface ISecurityPort {

    Long getIdUser(String token);
}
