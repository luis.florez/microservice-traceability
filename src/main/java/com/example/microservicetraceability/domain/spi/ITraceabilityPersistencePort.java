package com.example.microservicetraceability.domain.spi;

import com.example.microservicetraceability.domain.model.Traceability;

import java.util.List;

public interface ITraceabilityPersistencePort {

    void saveTraceability(Traceability traceability);

    List<Traceability> getTraceabilityByOrderAndCustomer(Long orderId, Long customerId);

    List<Traceability> getTraceabilityByOrder(Long orderId);
}
