package com.example.microservicetraceability.domain.usecase;

import com.example.microservicetraceability.domain.api.ITraceabilityServicePort;
import com.example.microservicetraceability.domain.model.Traceability;
import com.example.microservicetraceability.domain.model.TraceabilityTime;
import com.example.microservicetraceability.domain.model.UserRanking;
import com.example.microservicetraceability.domain.model.constant.TraceabilityUseCaseConstant;
import com.example.microservicetraceability.domain.spi.IOrdersPersistencePort;
import com.example.microservicetraceability.domain.spi.ISecurityPort;
import com.example.microservicetraceability.domain.spi.ITraceabilityPersistencePort;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class TraceabilityUseCase implements ITraceabilityServicePort {

    private final ITraceabilityPersistencePort traceabilityPersistencePort;
    private final ISecurityPort securityPort;
    private final IOrdersPersistencePort ordersPersistencePort;

    public TraceabilityUseCase(ITraceabilityPersistencePort traceabilityPersistencePort, ISecurityPort securityPort, IOrdersPersistencePort ordersPersistencePort) {
        this.traceabilityPersistencePort = traceabilityPersistencePort;
        this.securityPort = securityPort;
        this.ordersPersistencePort = ordersPersistencePort;
    }

    @Override
    public void saveTraceability(Traceability traceability) {
        traceabilityPersistencePort.saveTraceability(traceability);
    }

    @Override
    public List<Traceability> getTraceabilityByOrderAndCustomer(String token, Long orderId) {
        return traceabilityPersistencePort.getTraceabilityByOrderAndCustomer(orderId, securityPort.getIdUser(token));
    }

    @Override
    public List<TraceabilityTime> getTraceabilityTimesOrders(String token, Long restaurantId) {
        List<TraceabilityTime> traceabilityTimes = new ArrayList<>();
        List<Long> ordersId = ordersPersistencePort.getAllOrdersByStateDeliveredAndIdRestaurant(token, restaurantId);
        for ( Long orderId : ordersId ) {
            List<Traceability> traceabilityTime = traceabilityPersistencePort.getTraceabilityByOrder(orderId);
            LocalDateTime start = traceabilityTime.get(TraceabilityUseCaseConstant.LOG_OF_START_ORDER).getDate();
            LocalDateTime finish = traceabilityTime.get(TraceabilityUseCaseConstant.LOG_OF_FINISH_ORDER).getDate();
            traceabilityTimes.add(new TraceabilityTime(orderId, Duration.between(start, finish).toMinutes()));
        }
        return traceabilityTimes;
    }

    @Override
    public List<UserRanking> getRankingEmployees(String token, Long restaurantId) {
        List<UserRanking> userRankings = new ArrayList<>();
        List<Long> employeesId = ordersPersistencePort.getEmployeesByRestaurant(token, restaurantId);
        for ( Long employeeId : employeesId ) {
            List<Long> ordersId = ordersPersistencePort.getAllOrdersByStateDeliveredAndIdRestaurantAndIdEmployee(token, restaurantId, employeeId);
            List<TraceabilityTime> traceabilityTimes = new ArrayList<>();
            for ( Long orderId : ordersId ) {
                List<Traceability> traceabilityTime = traceabilityPersistencePort.getTraceabilityByOrder(orderId);
                LocalDateTime start = traceabilityTime.get(TraceabilityUseCaseConstant.LOG_OF_START_ORDER).getDate();
                LocalDateTime finish = traceabilityTime.get(TraceabilityUseCaseConstant.LOG_OF_FINISH_ORDER).getDate();
                traceabilityTimes.add(new TraceabilityTime(orderId, Duration.between(start, finish).toMinutes()));
            }
            int quantityOrders = TraceabilityUseCaseConstant.START_COUNT_OF_ORDERS;
            Long timeOrders = TraceabilityUseCaseConstant.START_ACCUMULATOR_OF_TIMES_OF_ORDERS;
            for ( TraceabilityTime traceabilityTime : traceabilityTimes ) {
                timeOrders += traceabilityTime.getTime();
                quantityOrders++;
            }
            if (quantityOrders == TraceabilityUseCaseConstant.START_COUNT_OF_ORDERS) userRankings.add(new UserRanking(employeeId, TraceabilityUseCaseConstant.START_ACCUMULATOR_OF_TIMES_OF_ORDERS));
            else userRankings.add(new UserRanking(employeeId, (timeOrders / quantityOrders)));
        }
        return userRankings;
    }
}
