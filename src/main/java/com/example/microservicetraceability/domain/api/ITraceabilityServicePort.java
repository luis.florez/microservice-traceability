package com.example.microservicetraceability.domain.api;

import com.example.microservicetraceability.domain.model.Traceability;
import com.example.microservicetraceability.domain.model.TraceabilityTime;
import com.example.microservicetraceability.domain.model.UserRanking;
import java.util.List;

public interface ITraceabilityServicePort {

    void saveTraceability(Traceability traceability);

    List<Traceability> getTraceabilityByOrderAndCustomer(String token, Long orderId);

    List<TraceabilityTime> getTraceabilityTimesOrders(String token, Long restaurantId);

    List<UserRanking> getRankingEmployees(String token, Long restaurantId);
}
